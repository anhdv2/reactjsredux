import React, { Component } from 'react';
import configureStore from '../src/reducers/configureStore'
import RouterApp from '../src/router/router'
import Provider from "react-redux/es/components/Provider";
import './asset/css/style.css';
import '../src/asset/css/bootstrap.min.css';
import 'antd/dist/antd.css';

class App extends Component {
  render() {
    return (
        <Provider store={configureStore()}>
            <RouterApp  />
        </Provider>
    );
  }
}

export default App;
