import {Modal, Button, Form, Input, Icon} from "antd";
import React, { Component } from "react";
import swal from "sweetalert";
import {LoginForm} from '../../../../service/APILending/ApiserviceLending'
import { connect } from "react-redux";
import {ReduxDatauserLogin} from '../../../../reducers/actionReducers/LoginRedux/actionUserlogin'
import { createStore } from 'redux'
import todoApp from '../../../../reducers/configureStore'
const store = createStore(todoApp)

class LoginModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false
        }
    }

    showModal = () => {
        this.setState({
            visible: true
        });
    }

    handleCancel = (e) => {
        this.setState({
            visible: false,
        });
    }

    handleSubmit = () => {
        let dataLogin = {username: this.email.input.value, password: this.password.input.value, }
        LoginForm(dataLogin).then(res => {
            if(res.err === 0){
                /*render data to redux*/
                this.props.ReduxDatauserLogin(res)
                swal("Thông báo", "Đăng nhập thành công!", "success");
            }else{
                swal("Thông báo", "Email hoặc password không chính xác!", "error");
            }
        })
    }
    componentDidMount() {
        console.log(store.subscribe(() => console.log(store.getState())), 444);
    }

    render() {
        return (
            <span>
                <Button onClick={this.showModal} className={'btn-default'} type={'primary'}>
                    {'Đăng nhập'}
                </Button>
                <Modal
                    maskClosable={false}
                    title="Đăng nhập"
                    style={{ textAlign: 'center', fontSize: '21px', textTransform: 'uppercase' }}
                    visible={this.state.visible}
                    footer={false}
                    onCancel={this.handleCancel}
                >
                    <Form onSubmit={this.handleSubmit}>
                        <Form.Item>
                            <Input ref={email => {
                                this.email = email;
                            }} prefix={<Icon type="phone" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Số điện thoại hoặc Email" />
                        </Form.Item>
                        <Form.Item>
                            <Input ref={password => {
                                this.password = password;
                            }} prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                        </Form.Item>
                        <div style={{height: '1px', borderTop: '1px dotted #999', margin: '20px 0 15px 0'}} />
                        <div style={{marginBottom: '20px'}}>
                            <Button type={'default'} onClick={this.handleSubmit}
                                    style={{backgroundColor: '#df7300', color: 'white', width: '100%', padding: '15px 0px 30px 0px'}} >Login</Button>
                        </div>
                        <div style={{marginBottom: '15px'}}>
                            <Button type={'primary'} style={{color: 'white', width: '100%', padding: '15px 0px 30px 0px'}} icon={'facebook'}>Đăng nhập bằng Facebook</Button>
                        </div>
                        <div style={{marginBottom: '30px'}}>
                            <span style={{color: 'black', width: '100%', padding: '15px 0px 30px 0px'}}>Quên mật khẩu</span>
                        </div>
                        <div style={{marginBottom: '20px'}}>
                            <span style={{color: 'black', width: '100%', padding: '15px 0px 30px 0px'}}>Chưa có tài khoản, đăng ký</span>
                        </div>
                    </Form>
                </Modal>
            </span>
        );
    }
}
const mapStateToProps = state => ({
    ...state
});
const mapDispatchToProps = dispatch => ({
    ReduxDatauserLogin: (dataUser) => dispatch(ReduxDatauserLogin(dataUser)),
});
export default connect(mapStateToProps, mapDispatchToProps)(LoginModal);
