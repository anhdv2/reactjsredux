import React, {Component} from "react";
import {Modal, Button, Form, Input, Icon} from "antd";
import ReCAPTCHA from "react-google-recaptcha";

class RegisterModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visible: false
        }
    }

    showModal = () => {
        this.setState({
            visible: true,
        });
    }

    handleOk = (e) => {
        this.setState({
            visible: false,
        });
    }

    handleCancel = (e) => {
        this.setState({
            visible: false,
        });
    }

    onChange(value) {
        console.log("Captcha value:", value);
    }

    render() {
        return (
            <span>
                <Button onClick={this.showModal} className={'btn-default'} type={'Default'}>
                    {'Đăng ký'}
                </Button>
                <Modal
                    title="Đăng ký"
                    maskClosable={false}
                    style={{textAlign: 'center', fontSize: '21px', textTransform: 'uppercase'}}
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                    footer={false}
                >
                    <Form onSubmit={this.handleSubmit}>
                        <Form.Item>
                            <Input style={{marginBottom: '20px'}}
                                   prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                                   placeholder="Họ và tên đầy đủ"/>
                            <Input style={{marginBottom: '20px'}}
                                   prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>} placeholder="Email"/>
                            <Input style={{marginBottom: '20px'}}
                                   prefix={<Icon type="phone" style={{color: 'rgba(0,0,0,.25)'}}/>} type="number"
                                   placeholder="Số điện thoại"/>
                            <Input style={{marginBottom: '20px'}}
                                   prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>} type="password"
                                   placeholder="Mật khẩu"/>
                            <Input style={{marginBottom: '20px'}}
                                   prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>} type="password"
                                   placeholder="Nhập mật khẩu nhắc lại"/>
                            <ReCAPTCHA
                                size="normal"
                                sitekey="6LcNJIYUAAAAAGd02kjpmnXJ9QFPNkXuSm9Eh76E"
                            />
                            <div style={{marginBottom: '20px', textAlign: 'left'}}>
                                <Input type="radio" value="1" checked/> Tôi muốn vay tiền
                            </div>
                            <div style={{height: '1px', borderTop: '1px dotted #999', margin: '20px 0 15px 0'}}/>
                            <div style={{marginBottom: '20px'}}>
                                <Button type={'default'} style={{
                                    backgroundColor: '#df7300',
                                    color: 'white',
                                    width: '100%',
                                    padding: '15px 0px 30px 0px'
                                }}>Đăng ký ngay</Button>
                            </div>
                            <div style={{marginBottom: '15px'}}>
                                <Button type={'primary'}
                                        style={{color: 'white', width: '100%', padding: '15px 0px 30px 0px'}}
                                        icon={'facebook'}>Đăng ký Facebook</Button>
                            </div>
                            <div style={{marginBottom: '30px'}}>
                                <a style={{color: 'black', width: '100%', padding: '15px 0px 30px 0px'}}>Đăng nhập</a>
                            </div>
                        </Form.Item>
                    </Form>
                </Modal>
            </span>
        );
    }
}

export default RegisterModal
