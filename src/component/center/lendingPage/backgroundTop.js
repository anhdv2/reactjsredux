import React, {Component} from 'react';
import { Slider } from 'antd';

import icPhone from '../../../asset/img/ic-phone.png'
import icThoigianvay from '../../../asset/img/ic-thoigianvay.png'
import icMail from '../../../asset/img/ic-mail.png'

class Footer extends Component {
    constructor(props){
        super(props)
        this.state = {
            disabled: false,
            valueSliderNumber: 0
        };
    }

    changeSliderLending = (value) =>{
        this.setState({
            valueSliderNumber: value
        })
    }

    render() {
        return (
            <div>
                <div className="center-box-register">
                    <div className="box-regis-form-control">
                        <div className="box-form-regis">
                            <form className="form-regis-box-regis">
                                <div className="header-box-regis-title">ĐĂNG KÝ VAY VÀ HỖ TRỢ TÀI CHÍNH</div>
                                <div className="header-box-regis-description">Đăng ký vay ngay chỉ trong 30 phút bằng
                                    việc cung cấp các thông tin cá nhân và nhu cầu vay
                                </div>
                                <div className="input-form-reigister-write">
                                    <div className="row">
                                        <div className="input-left input-phone-form-lending">
                                            <div className="title-input-phone-lending"><span>Số di động</span></div>
                                            <div className="input-tag-lending icon-and-input-lending">
                                                <div className="width-lending-img-lending">
                                                    <img src={icPhone} className="icon-input-lending" alt=""/>
                                                </div>
                                                <input type="number"
                                                       className="input-type-text-form button-name-input-lending-ph"
                                                       placeholder="Nhập số điện thoại"/>
                                            </div>
                                            {/*<div style={'border-bottom: 1px solid #deebf7;margin-bottom: 34px'}></div>*/}
                                        </div>
                                        <div className="input-right input-time-borrow-form-lending">
                                            <div className="title-input-phone-lending"><span>Thời gian vay</span></div>
                                            <div className="input-tag-lending icon-and-input-lending">
                                                <div className="width-lending-img-lending">
                                                    <img src={icThoigianvay} className="icon-input-lending" alt=""/>
                                                </div>
                                                <select className="input-type-text-form select-input-form-time-borrow">
                                                    <option value="1">1 tháng</option>
                                                    <option value="3">3 tháng</option>
                                                    <option value="6">6 tháng</option>
                                                    <option value="12">12 tháng</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="input-left input-email-form-lending">
                                            <div className="title-input-phone-lending"><span>Email</span></div>
                                            <div className="input-tag-lending icon-and-input-lending">
                                                <div className="width-lending-img-lending">
                                                    <img src={icMail} className="icon-input-lending" alt=""/>
                                                </div>
                                                <input required type="text"
                                                       className="input-type-text-form button-email-input-lending"
                                                       placeholder="Nhập địa chỉ email"/>
                                            </div>
                                        </div>
                                        <div className="input-right input-totalborrow-form-lending">
                                            <div className="title-input-phone-lending"><span>Số tiền muốn vay</span>
                                            </div>
                                            <div className="input-tag-lending">
                                                <div className="slider-line-lending">
                                                    <Slider onChange={this.changeSliderLending} defaultValue={this.state.valueSliderNumber} max={70000} step={500} disabled={this.state.disabled} />
                                                </div>
                                            </div>
                                            <div className="render-number-borrow-total-rewrite">{this.state.valueSliderNumber}</div>
                                        </div>
                                    </div>
                                </div>
                                <div className="button-submit-form-in-lending">
                                    <span>ĐĂNG KÝ</span>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Footer;
