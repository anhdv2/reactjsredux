import React, {Component} from 'react';
import BackgroundTop from './backgroundTop'
import WhyPicktruemoney from './whyPicktruemoney'
import PartnerListicon from './partnerListicon'

class boxLending extends Component {
    constructor(props){
        super(props)
        this.state = {
            disabled: false,
            valueSliderNumber: 0
        };
    }

    render() {
        return (
            <div>
                <BackgroundTop />
                <WhyPicktruemoney />
                <PartnerListicon />
            </div>
        );
    }
}

export default boxLending;
