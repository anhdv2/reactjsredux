import React, {Component} from 'react';
import iclogotpbank from "../../../asset/img/ic-logo-tpbank.png";
import iclogomb from "../../../asset/img/ic-logo-mb.png";
import iclogobv from "../../../asset/img/ic-logo-bv.png";
import iclogovcb from "../../../asset/img/ic-logo-vcb.png";
import iclogohd from "../../../asset/img/ic-logo-hd.png";
import iclogodonga from "../../../asset/img/ic-logo-donga.png";
import iclogoevn from "../../../asset/img/ic-logo-evn.png";
import iclogokplus from "../../../asset/img/ic-logo-kplus.png";
import iclogopru from "../../../asset/img/ic-logo-pru.png";
import iclogofpt from "../../../asset/img/ic-logo-fpt.png";
import iclogopti from "../../../asset/img/ic-logo-pti.png";
import iclogovna from "../../../asset/img/ic-logo-vna.png";
import iclogoavg from "../../../asset/img/ic-logo-avg.png";

class partnerListicon extends Component {
    render() {
        return (
            <div className="list-bank-content-lending">
                <div className="header-title-bank-content-lending">
                    <span>Đối tác của chúng tôi</span>
                </div>
                <div className="list-content-bank-lending">
                    <div className="item-lending-bank-icon"><img src={iclogotpbank}
                                                                 className="icon-bank-item-in-list" alt=""/></div>
                    <div className="item-lending-bank-icon"><img src={iclogomb} className="icon-bank-item-in-list"
                                                                 alt=""/></div>
                    <div className="item-lending-bank-icon"><img src={iclogobv} className="icon-bank-item-in-list"
                                                                 alt=""/></div>
                    <div className="item-lending-bank-icon"><img src={iclogovcb} className="icon-bank-item-in-list"
                                                                 alt=""/></div>
                    <div className="item-lending-bank-icon"><img src={iclogohd} className="icon-bank-item-in-list"
                                                                 alt=""/></div>
                    <div className="item-lending-bank-icon"><img src={iclogodonga}
                                                                 className="icon-bank-item-in-list" alt=""/></div>
                    <div className="item-lending-bank-icon"><img src={iclogotpbank}
                                                                 className="icon-bank-item-in-list" alt=""/></div>
                </div>
                <div className="list-content-bank-lending">
                    <div className="item-lending-bank-icon"><img src={iclogoevn} className="icon-bank-item-in-list"
                                                                 alt=""/></div>
                    <div className="item-lending-bank-icon"><img src={iclogokplus}
                                                                 className="icon-bank-item-in-list" alt=""/></div>
                    <div className="item-lending-bank-icon"><img src={iclogopru} className="icon-bank-item-in-list"
                                                                 alt=""/></div>
                    <div className="item-lending-bank-icon"><img src={iclogofpt} className="icon-bank-item-in-list"
                                                                 alt=""/></div>
                    <div className="item-lending-bank-icon"><img src={iclogopti} className="icon-bank-item-in-list"
                                                                 alt=""/></div>
                    <div className="item-lending-bank-icon"><img src={iclogovna} className="icon-bank-item-in-list"
                                                                 alt=""/></div>
                    <div className="item-lending-bank-icon"><img src={iclogoavg} className="icon-bank-item-in-list"
                                                                 alt=""/></div>
                </div>
            </div>
        );
    }
}

export default partnerListicon;
