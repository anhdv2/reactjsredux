import React, {Component} from 'react';
import icDuyetvay from "../../../asset/img/ic-duyetvay.png";
import icToiuuvay from "../../../asset/img/ic-toiuuvay.png";
import icMangluoi from "../../../asset/img/ic-mangluoi.png";
import icTincay from "../../../asset/img/ic-tincay.png";

class whyPicktruemoney extends Component {
    render() {
        return (
            <div className="box-content-why-finhub-true">
                <div className="row-header-content-why-findhub-true">
                    <span>Tại sao chọn Finhub Truemoney?</span>
                </div>
                <div className="box-footer-content-why-findhub-true">
                    <div className="row-1">
                        <div className="box-content-item-findhub-true">
                            <div className="img-true-why-findhub-true">
                                <div className="box-img-finhub-true">
                                    <img src={icDuyetvay} className="img-findhub-true-left-content" alt={''}/>
                                </div>
                                <div className="text-title-top-why-findhub-true">Duyệt vay nhanh chóng</div>
                            </div>
                            <div className="title-content-render-view">
                                <div className="text-description-top-why-findhub-true">Không cần tài sản bảo đảm,
                                    không cần chứng minh thu nhập, đăng ký và duyệt đơn vay chỉ trong vài giờ.
                                </div>
                            </div>
                        </div>
                        <div className="box-content-item-findhub-true">
                            <div className="img-true-why-findhub-true">
                                <div className="box-img-finhub-true">
                                    <img src={icToiuuvay} className="img-findhub-true-left-content" alt={''}/>
                                </div>
                                <div className="text-title-top-why-findhub-true">Tối ưu khoản vay</div>
                            </div>
                            <div className="title-content-render-view">
                                <div className="text-description-top-why-findhub-true">Nhận được tư vấn hiệu quả từ
                                    đội ngũ telesale chuyên nghiệp để tối ưu hóa khoản vay mà bạn muốn.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row-2">
                        <div className="box-content-item-findhub-true">
                            <div className="img-true-why-findhub-true">
                                <div className="box-img-finhub-true">
                                    <img src={icMangluoi} className="img-findhub-true-left-content" alt={''}/>
                                </div>
                                <div className="text-title-top-why-findhub-true">Mạng lưới rộng khắp</div>
                            </div>
                            <div className="title-content-render-view">
                                <div className="text-description-top-why-findhub-true">Với mạng lưới kết nối rộng
                                    khắp toàn quốc cùng với hệ thống hoạt động 24/7, chúng tôi luôn sẵn sàng đáp ứng
                                    mọi yêu cầu của bạn.
                                </div>
                            </div>
                        </div>
                        <div className="box-content-item-findhub-true">
                            <div className="img-true-why-findhub-true">
                                <div className="box-img-finhub-true">
                                    <img src={icTincay} className="img-findhub-true-left-content" alt={''}/>
                                </div>
                                <div className="text-title-top-why-findhub-true">Minh bạch, tin cậy</div>
                            </div>
                            <div className="title-content-render-view">
                                <div className="text-description-top-why-findhub-true">Mọi khoản vay và quy trình
                                    vay đều được thể hiện trong sáng, minh bạch trên hệ thống để các bên liên quan
                                    cùng theo dõi.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default whyPicktruemoney;
