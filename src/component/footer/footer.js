import React, { Component } from 'react';
import { Link } from "react-router-dom";

import FacebookIcon from '../../asset/img/Facebook-Icon.png'
import googlePlus from '../../asset/img/google-plus-icon--circle-iconset--martz90-20.png'
import linkedinCircle from '../../asset/img/linkedin_circle-512.png'
import viberIconV2 from '../../asset/img/viber-iconV2.png'
import YoutubeCircle from '../../asset/img/Youtube_circle.svg'
import icLienket1 from '../../asset/img/ic-lienket1.png'
import icLienket2 from '../../asset/img/ic-lienket2.png'
import icLienket3 from '../../asset/img/ic-lienket3.png'
import icLienket4 from '../../asset/img/ic-lienket4.png'
import icLienket5 from '../../asset/img/ic-lienket5.png'
import icLienket6 from '../../asset/img/ic-lienket6.png'
import icHoptac1 from '../../asset/img/ic-hoptac1.png'
import icHoptac2 from '../../asset/img/ic-hoptac2.png'
import icChungchi from '../../asset/img/ic-chungchi.png'

class Footer extends Component {
    render() {
        return (
            <div>
                <div className="item-list-menu-footer-banner">
                    <div className="box-menu-footer-all">
                        <div className="row-menu-1">
                            <div className="column-one-menu-footer-banner">
                                <div className="text-style-menu-banner first-menu-column-banner">Cá nhân</div>
                                <div className="text-style-menu-banner second-menu-column-banner">Ví điện tử</div>
                                <div className="text-style-menu-banner second-menu-column-banner">Mạng luới giao dịch
                                </div>
                                <div className="text-style-menu-banner second-menu-column-banner">Điều khoản DV</div>
                                <div className="text-style-menu-banner second-menu-column-banner">Chính sách & quyền
                                    riêng tư
                                </div>
                                <div className="text-style-menu-banner second-menu-column-banner">Biểu phí</div>
                            </div>
                            <div className="column-one-menu-footer-banner">
                                <div className="text-style-menu-banner first-menu-column-banner">Giải pháp kinh doanh
                                </div>
                                <div className="text-style-menu-banner second-menu-column-banner">Đăng ký làm đại lý
                                </div>
                                <div className="text-style-menu-banner second-menu-column-banner">DV cung cấp</div>
                                <div className="text-style-menu-banner second-menu-column-banner">Chính sách & điều
                                    khoản
                                </div>
                                <div className="text-style-menu-banner second-menu-column-banner">Biểu phí</div>
                            </div>
                        </div>
                        <div className="row-menu-2">
                            <div className="column-one-menu-footer-banner">
                                <div className="text-style-menu-banner first-menu-column-banner">Hỗ trợ</div>
                                <div className="text-style-menu-banner second-menu-column-banner">Cá nhân</div>
                                <div className="text-style-menu-banner second-menu-column-banner">Kinh doanh</div>
                                <div className="text-style-menu-banner second-menu-column-banner">Developer</div>
                                <div className="text-style-menu-banner second-menu-column-banner">Điều khoản, giấy
                                    phép
                                </div>
                                <div className="text-style-menu-banner second-menu-column-banner">Liên hệ hỗ trợ</div>
                            </div>
                            <div className="column-one-menu-footer-banner">
                                <div className="text-style-menu-banner first-menu-column-banner">Về chúng tôi</div>
                                <div className="text-style-menu-banner second-menu-column-banner">Liên hệ</div>
                                <div className="text-style-menu-banner second-menu-column-banner">Công ty</div>
                                <div className="text-style-menu-banner second-menu-column-banner">Tuyển dụng</div>
                                <div className="text-style-menu-banner second-menu-column-banner">Tin tức</div>
                            </div>
                        </div>
                        <div className="column-one-menu-footer-banner responsive-menu-footer-banner">
                            {/*<div class="text-style-menu-banner first-menu-column-banner">Đăng ký nhân tin</div>
                            <div class="input-write-to-accept-sent-information">
                              <input type="text" name="sent-..." class="email-sent-mail-input" placeholder="Nhập email...">
                              <span class="glyphicon glyphicon-send" style="color: white"></span>
                            </div>*/}
                            <div className="title-public-internet">
                                <span>Liên kết</span>
                            </div>
                            <div className="list-icon-internet-public">
                                <Link to={'#'} className="icon-internet-public" href="#"><img src={FacebookIcon} alt="" /></Link>
                                <Link to={'#'} className="icon-internet-public" href="#"><img src={googlePlus} alt="" /></Link>
                                <Link to={'#'} className="icon-internet-public" href="#"><img src={linkedinCircle} alt="" /></Link>
                                <Link to={'#'} className="icon-internet-public" href="#"><img src={viberIconV2} alt="" /></Link>
                                <Link to={'#'} className="icon-internet-public" href="#"><img src={YoutubeCircle} alt="" /></Link>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="footer-icon-country-in-lending">
                    <div className="box-content-all-country-andoi-partner">
                        <div className="box-icon-country-lending">
                            <div className="header-tille-country-lending">
                                <span>Mạng lưới TrueMoney</span>
                            </div>
                            <div className="list-icon-country-lending">
                                <img src={icLienket1} className="icon-country-img-lending" alt="" />
                                <img src={icLienket2} className="icon-country-img-lending" alt="" />
                                <img src={icLienket3} className="icon-country-img-lending" alt="" />
                                <img src={icLienket4} className="icon-country-img-lending" alt="" />
                                <img src={icLienket5} className="icon-country-img-lending" alt="" />
                                <img src={icLienket6} className="icon-country-img-lending" alt="" />
                            </div>
                        </div>
                        <div className="box-icon-partner-lending">
                            <div className="header-tille-partner-lending">
                                <span>Hợp tác bởi</span>
                            </div>
                            <div className="list-icon-partner-lending">
                                <img src={icHoptac1} className="icon-partner-img-lending" alt="" />
                                <img src={icHoptac2} className="icon-partner-img-lending-mog" alt="" />
                            </div>
                        </div>
                        <div className="box-icon-partner-lending">
                            <div className="header-tille-partner-lending">
                                <span>Chứng chỉ</span>
                            </div>
                            <div className="list-icon-partner-lending">
                                <img src={icChungchi} className="icon-partner-img-lending-pci" alt="" />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="box-information-to-company-all">
                    <div className="box-style-information-company">
                        <div className="box-header-title-information-company">
                          <span>Văn phòng giao dịch</span>
                        </div>
                        {/*<div className="box-list-all-company-infor">
                          <div className="information-company-in-hn">
                            <div className="name-country-hn"><span>TP. Hà Nội</span></div>
                            <div className="description-country-hn"><span>Địa chỉ: Tầng 12. Tòa nhà Sông Hồng, Số 165 Thái Hà, Quận Đống Đa, Hà Nội.</span></div>
                            <div className="phone-country-hn"><span>(+84) 4 3640 8774</span></div>
                          </div>
                          <div className="information-company-in-hn">
                            <div className="name-country-hn"><span>TP. Đà Nẵng</span></div>
                            <div className="description-country-hn"><span>Địa chỉ: Số 69 c-hoptac1.ddea6ed.pngQuang Trung, Phường Hải Châu 1, QUanạ Hải Châu, TP. Đà Nẵng</span></div>
                            <div className="phone-country-hn"><span>(+84) 2363 888 556</span></div>
                          </div>
                          <div className="information-company-in-hn">
                            <div className="name-country-hn"><span>TP. Hồ Chí Minh</span></div>
                            <div className="description-country-hn"><span>Địa chỉ: 927/1 Đường CMT8, Quận Tân Bình, TP. Hồ Chí Minh</span></div>
                            <div className="phone-country-hn"><span>(+84) 8 39700377</span></div>
                          </div>
                        </div>*/}
                        <div className="all-right"><img alt={''} src="https://img.icons8.com/material-outlined/1600/copyright.png" width="18" />
                            TrueMoney 2018. All rights reserved.
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Footer;
