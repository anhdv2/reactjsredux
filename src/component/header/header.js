import React, { Component } from 'react';
import icCustomerService from '../../asset/img/ic-customer-service.png'
import icLogo from '../../asset/img/ic-logo-1.png'
import LoginModal  from '../center/lendingPage/ModalLending/LoginModal'
import RegisterModal  from '../center/lendingPage/ModalLending/RegisterModal'

class Header extends Component {
    constructor(props){
        super(props)
        this.state = {
            visible: true
        }
    }

    render() {
        return (
            <div>
                <header className="fixed isShowMenu name-header-top-site">
                    <div className="logo-header-top-menu-l">
                        <div className="icon-true-money">
                            <a href="/"><img alt={''}  src={icLogo} /></a>
                        </div>
                        <div className="phone-number-icon">
                            <div className="icon-phone-true">
                                <a href="tel:02473008168">
                                    <img src={icCustomerService} alt={''}  /></a>
                            </div>
                            <div className="phone-number-true-header-logo">
                                <a href="tel:02473008168">0247 300 8168</a>
                            </div>
                        </div>
                    </div>
                    <ul className="nav navbar-nav navbar-right">
                        <li className="dropdown">
                        </li>
                    </ul>
                <div className="menu-header-top-site">
                    <LoginModal />
                    <RegisterModal />
                </div>
                </header>
            </div>
        );
    }
}

export default Header;
