import axios from 'axios'

/*axit post*/
export async function axiosPOST(Url, param = {}, header = {}) {
    return await axios.post(Url,  param,{ headers: header}).then(res => {
        return res.data;
    }).catch(err => {
        return err.response;
    })
}

/*axit get*/
export async function axiosGET(Url, header = {}) {
    return await axios.get(Url,  { headers: header} ).then(res => {
        return res.data;
    }).catch(err => {
        return err.response;
    })
}

/*axit delete*/
export async function axiosDELETE(Url, header = {}) {
    return await axios.delete(Url,  { headers: header} ).then(res => {
        return res.data;
    }).catch(err => {
        return err.response;
    })
}

/*axit put*/
export async function axiosPUT(Url, header = {}) {
    return await axios.put(Url,  { headers: header} ).then(res => {
        return res.data;
    }).catch(err => {
        return err.response;
    })
}
