export default (state, action) => {
    switch (action) {
        case "userLogin":
            return {
                user: action.dataUser
            };
        default:
            return state;
    }
};
