import { createStore } from "redux";
import rotateReducer from "./actionReducers/LoginRedux/StoreRedux";

function configureStore(state = { user: "" }) {
    return createStore(rotateReducer,state);
}

export default configureStore;
