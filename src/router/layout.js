import React from 'react'
import Header from '../component/header/header'
import Footer from '../component/footer/footer'

// import '../assets/css/bootstrap.min.css'
// import '../assets/css/antd.min.css'
// import '../assets/css/style.css'

const Layout = (props) => {
    return (
        <div>
            <Header />
            <div>{props.children}</div>
            <Footer />
        </div>
    )
}

export default Layout
