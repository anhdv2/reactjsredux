import React, { Component } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'
import Layout from '../router/layout'
import BoxLending from '../component/center/lendingPage/boxLending'

class RouterApp extends Component {
    constructor(props){
        super(props)
        this.state = {
            isAuthenticated: true,
            urlBase: ''
        }
    }

    render() {
        const PrivateRoute = ({ component: Component, ...rest }) => (
            <Route {...rest} render={(props) => (
                this.state.isAuthenticated === true
                    ? <Component {...props} />
                    : <Redirect to='/login' />
            )} />
        )
        return (
            <BrowserRouter>
                <Switch>
                    <Layout>
                        <PrivateRoute path="/" component={BoxLending}/>
                        <PrivateRoute path="/login" component={BoxLending}/>
                        <PrivateRoute path='/protected' component={BoxLending}/>
                    </Layout>
                </Switch>
            </BrowserRouter>
        );
    }
}

export default RouterApp;
