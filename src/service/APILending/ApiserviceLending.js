import {axiosPOST} from '../../helper/TypeAxios'
const URL_BASE = 'http://finhub.truemoney.local/auth/api'

export function LoginForm (datalogin) {
    let keyObject = {
        client_id: process.env.REACT_APP_CLIENT_ID,
        client_secret: process.env.REACT_APP_CLIENT_SECRET,
        grant_type: 'password'
    }
    datalogin =  Object.assign(keyObject, datalogin);
    return axiosPOST(URL_BASE + '/v1/login', datalogin, {})
}
