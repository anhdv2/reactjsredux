import {BaseAPI} from '../base'

export class AuthAPI {
    /**
     * User API construction
     * @returns {AxiosInstance}
     */
    constructor (context = null) {
        this.api = new BaseAPI(context, process.env.authUrl)
    }

    async login (params) {
        let result = await this.api.post('/v1/login', params)
        return result
    }

    async loginWithSocial (params) {
        let result = await this.api.post('/v1/login-open-id', params)
        return result
    }

    async updateInfo (params) {
        delete params['email']
        let result = await this.api.put('/v1/user', params)
        return result
    }

    async register (params) {
        let result = await this.api.post('/v1/register', params)
        return result
    }

    async confirmEmail (params) {
        let result = await this.api.post('/v1/confirm-verify-email', params)
        return result
    }

    async forgotPassword (params) {
        let result = await this.api.post('/v1/forgot-password', params)
        return result
    }

    async resetPassword (params) {
        let result = await this.api.post('/v1/reset-password', params)
        return result
    }
    async VerifyToken (params) {
        let result = await this.api.post('/v1/confirm-verify-phone', params)
        return result
    }
    async ResentVerifyToken (params) {
        let result = await this.api.post('/v1/resent-verify-phone-request', params)
        return result
    }
}
